<?php
$title = "Nous contacter";
require_once 'config.php';
require_once 'functions.php';
date_default_timezone_set('Europe/Paris');
if (isset($_GET['heure'])) {
    $heure = (int) $_GET['heure'];
} else {
    $heure = date('G');
};
if (isset($_GET['jour'])) {
    $jour = (int) $_GET['jour'];
} else {
    $jour = date('N') - 1;
};
$creneaux = CRENEAUX[$jour];
$ouvert = in_creneaux($heure, $creneaux);
$color = 'green';
if (!$ouvert) {
    $color = 'red';
}
require 'elements/header.php';
?>

<div class="col-12 body_contact">
    <div class="row">
        <div class="col-md-8">
            <form id="ici" action="pages/traitement.php" method="post">
                <div class="formulaire">
                    <div class="toto">
                        <label for="name">Nom :</label>
                        <input type="text" id="name" name="user_name" required minlength="4">
                    </div>

                    <div class="toto">
                        <label for="mail">e-mail :</label>
                        <input type="email" id="mail" name="user_mail" required>
                    </div>

                    <div class="toto">
                        <label for="phone">Téléphone :</label>
                        <input type="text" id="phone" name="user_phone" required minlength="4">
                    </div>

                    <div class="toto">
                        <label for="subject">Sujet :</label>
                        <input type="text" id="subject" name="subject" required minlength="4">
                    </div>

                    <div class="toto">
                        <label for="msg">Message :</label>
                        <textarea id="msg" name="user_message" required></textarea>
                    </div>

                    <div class="toto">
                        <button class="button" type="submit">Envoyer le message</button>
                    </div>
                </div>
            </form>
        </div>
        <div class="col-md-4 title">
            <h2>Horaires d'Ouverture</h2>
            <?php if ($ouvert) : ?>
                <div class="alert alert-success">La salle sera ouverte</div>
            <?php else : ?>
                <div class="alert alert-danger">La salle sera fermée</div>
            <?php endif ?>
            <form action="contact.php" method="GET">
                <div class="form-group">
                    <?php printf(select('jour', $jour, JOURS)) ?>

                </div>
                <div class="form-group">
                    <input class="form-control" type="number" name="heure" value="<?php printf($heure); ?>">
                </div>
                <button type="submit" class="btn btn-danger">Voir si la salle est ouverte</button>
            </form>

            <ul class="mt-3">
                <?php foreach (JOURS as $key => $jour) : ?>
                    <li class="list-unstyled" <?php if ($key + 1 === (int) date('N')) : ?>style="color:<?php printf($color); ?>" <?php endif ?>>
                        <strong><?php printf($jour); ?></strong> :
                        <?php printf(creneaux_html(CRENEAUX[$key])); ?>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
</div>
</div>
<?php
require 'elements/footer.php';
?>