    <?php
    $title = "Accueil";
    include("elements/header.php");
    ?>
    <div class="test">
        <section class="accueil">
            <div class="container-index">
                <div class="video-box">
                    <video class="video" src="video/Gym-Workout-Cinematic-Trailer.mp4" controls>HELLO BOY!</video>
                </div>
            </div>
            <div class="pub">
                <p>Come to the gym, to discover your origins ! </p>
                <a href="apropos.php">More</a>
            </div>
        </section>
    </div>
    <?php
    include("elements/footer.php");
    ?>