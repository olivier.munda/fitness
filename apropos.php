<?php
$title = "A porpos";
include('elements/header.php');
?>



<div class="container apropos">
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="image">
                <img src="img/fitness_woman_3.png" alt="" class="img-fluid">
            </div>
        </div>
        <div class="col-lg-6 col-md-12 title">
            <h2 class="text-uppercase pt-5">
                <span>A propos de nous</span></h2>
            <div class="paragraph">
                <p class="para">La salle de sport se veut proche de ses adhérents : elle est ouverte 7 jours sur 7, de 6 h à 23 h avec une présence à l'acceuil assurée tous les jours entre 8h-12h et 14h-19h. Pour répondre à ces besoins, le club dispose d’une équipe de 5 personnes dynamiques et à l’écoute.</p>
                <p class="para"> Des programmes sportifs sur-mesure peuvent être mis en place à la demande. La taille de la salle est de 1200 m². Un grand plateau qui permet de disposer de différents univers mais aussi d’un espace cross-training, d’une plateforme vibrante SISMO, d’un espace détente avec distributeur de boissons YANGA, etc...</p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12 title">
            <h2 class="text-uppercase pt-5">
                <span>Nous contacter</span></h2>
            <div class="paragraph">
                <p class="para">Vous pouvez nous joindre par téléphone au 00 00 00 00 00 <br>On vous répond du lundi au vendredi de 9h00 à 12h00 et de 14h00 à 17h00</p>
                <p class="para">Une question ? Un truc à nous dire ? C'est <a id="contact" href="contact.php">ici</a> que ça se passe. On vous répond au plus vite, que ça soit pour des infos sur la salle, les machines, les coachs ou tout autre renseignement.</p>
            </div>
        </div>
        <div class="col-lg-6 col-md-12">
            <div class="image">
                <img src="./img/img3.png" alt="" class="img-fluid">
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-12">
            <div class="image">
                <img src="img/map.jpg" alt="" class="img-fluid bt-3">
            </div>
        </div>
        <div class="col-lg-6 col-md-12 title">
            <h2 class="text-uppercase pt-5">
                <span>Où nous trouver</span></h2>
            <div class="paragraph">
                <p class="para" id="loc">25 rue du Temple <br> 07160 Le Cheylard</p>
            </div>
        </div>
    </div>

</div>

<?php
include('elements/footer.php');
?>